You will need two virtual machines in the task

Install Ansible on one virtual machine, the other machine will be the destination on which specific tasks should be performed

Ansible must be logged in via ssh-key on the other machine
* The taks are:
    * change timezone to Europe/Spain (1 point)
    * change hostname (1 point)
    * Install the following tools with one Task: (2 points)
        * vim
        * telnet
        * htop
        * iotop
        * atop
        * mc
        * traceroute
        * unzip
    * Create motd(message of the day). So when logging in with ssh, that the following information should be output by logging in: (2 points)
        * IP address of the machine
        * hostname
        * What OS is installed
The given points must be dynamic (if we enter another machine - relevant information must be displayed)
* In the opt directory, create a folder named academy (1 point)
* copy 3 files to this directory (with the help of a loop), the names of the files are: (2 points)
        * foo
        * bar
        * baz